package fr.floxiik.totem;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class TotemGiveCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(strings.length < 1) {
            //Send message d'erreur
        } else if(strings[0].equals("give")) {
            if(strings.length < 2) {
                //Pas assez d'arguments
            } else {
                try {
                    Bukkit.getPlayer(strings[1]).getInventory().addItem(getTotemItem());
                    return true;
                } catch (NullPointerException npe) {
                    //Joueur existe pas
                }
            }
        }
        return false;
    }

    private ItemStack getTotemItem() {
        FileConfiguration configuration = Totem.getInstance().getConfig();
        ItemStack totem = new ItemStack(Material.matchMaterial(configuration.getString("totem-material")), 1);
        ItemMeta itemMeta = totem.getItemMeta();
        itemMeta.setDisplayName(
                ChatColor.translateAlternateColorCodes('&', configuration.getString("totem-name")));
        ArrayList<String> lores = new ArrayList<>();
        for(String lore : configuration.getStringList("totem-lore"))
            lores.add(ChatColor.translateAlternateColorCodes('&', lore));
        itemMeta.setLore(lores);
        totem.setItemMeta(itemMeta);
        return totem;
    }
}
