package fr.floxiik.totem;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class Totem extends JavaPlugin {

    private static Totem instance;

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();

        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new TotemActionListener(), this);
        getCommand("totem").setExecutor(new TotemGiveCommand());
    }

    @Override
    public void onDisable() {
    }

    public static Totem getInstance() {
        return instance;
    }
}
