package fr.floxiik.totem;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

public class TotemActionListener implements Listener {

    private final String totemName = ChatColor.translateAlternateColorCodes('&', Totem.getInstance().getConfig().getString("totem-name"));

    @EventHandler
    public void onDeath(EntityDamageEvent entityDamageEvent) {
        if(entityDamageEvent.getEntity() instanceof Player) {
            Player player = (Player) entityDamageEvent.getEntity();
            if(player.getHealth() - entityDamageEvent.getFinalDamage() <= 0 && verifyTotem(player)) {
                entityDamageEvent.setCancelled(true);
                player.setHealth(10D);
            }
        }
    }

    private boolean verifyTotem(Player player) {
        for(ItemStack item : player.getInventory().getContents()) {
            try {
                if(item.getItemMeta().getDisplayName().equals(totemName)) {
                    player.getInventory().remove(item);
                    return true;
                }
            } catch (NullPointerException ignored) {}
        }
        return false;
    }
}
